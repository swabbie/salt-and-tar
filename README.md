# Salt and Tar
I love Salt and Tar. They are a young couple who are building a sailboat. They are youtube content providers. This web app is part of the bigger concept of Swabbie. This part will focus on the channel itself, and not on the larger app.

The idea is that a youtube content provider in a particular group, can generate a site and include it in the larger streaming service.

In this example, Salt and Tar will have there own site. From here they will be able to link the media from YouTube, manage members, manage a store, manage reservations.

Any members here as well as media will be applied to Swabbie. Store products can be purchased with one click, while viewing video content, if enabled.

Below you can find the tech stack used for this project.

## Figma
I have designed the landing page concept in Figma. That design remains a work in progress.

## WebFlow
I have used WebFlow to quickly create the landing page from the Figma design. This will be exported and used to create the ReactJs app.

## ReactJs
I am using React to build out the frontend. Overall, I believe that NextJs will serve this project better, however I want to sharpen my React skills, use the new React version 18, and have the experience of building out these components. I have used Create React App to get started. This project will also use Typescript. Find out more here:
[React-Typescript](https://create-react-app.dev/docs/adding-typescript/)

## Typescript
These will be helpful with typescript.
 * [Cheat Sheet](https://github.com/typescript-cheatsheets/react)
 * [Nathan Cheat Sheet](https://blog.bitsrc.io/react-with-typescript-cheatsheet-9dd891dc5bfe)

## Eslint
I added eslint to keep the code tight. I used this medium article to set it up.
[ESLint in React with Typescript](https://andrebnassis.medium.com/setting-eslint-on-a-react-typescript-project-2021-1190a43ffba)

## Tailwindcss
I am using Tailwind css to style this project. Tailwind css differs from Material UI and Bootstrap in that you do not use pre-made elements. Tailwind is a css system that allows one to easily build your own elements. Find more info here: [TailwindCss](https://tailwindcss.com)
