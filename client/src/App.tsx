import React from 'react';
import './App.css';

import Navbar from './components/nav/Nav';
import Hero from './components/hero/Hero';

const App:React.FC = () => (
  <div className="flex justify-center">
    <Navbar />
    <Hero />
  </div>
);

export default App;
