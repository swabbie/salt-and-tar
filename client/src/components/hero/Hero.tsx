import React from 'react';

import Group8 from '../../images/group_8.png';
import Group28 from '../../images/group_28.svg';
import './hero-style.css';

const Hero: React.FC = () => (
  <section>
    <div className="hero-container">
      <div className="hero-wrap">
        <div className="hero-info-col">
          <div className="hero-content">
            <h1 className="hero-title">Salt &#38; Tar</h1>
            <h4 className="hero-subTitle">Journey of a wooden boat</h4>
            <div className="hero-btns-wrap">
              <button type="button" className="button">Learn More</button>
              <button type="button" className="button-2">Support Rediviva</button>
            </div>
          </div>
        </div>
        <div className="hero-img-col">
          <img src={Group8} alt="Garret and Ruth" />
        </div>
      </div>
      <img src={Group28} alt="decorative divide" />
    </div>
  </section>
);

export default Hero;
