/* eslint-disable react/jsx-closing-bracket-location */
import React, { useState } from 'react';
import BrandLogo from '../../images/logo.svg';
import NavOpen from '../../images/nav.svg';
import './nav-style.css';

const Navbar:React.FC = () => {
  const [navView, setNavView] = useState<boolean>(false);
  return (
    <section className="header-section">
      <header className="header">
        <div className="navbar">
          <div className="container">
            <div className="brand">
              <img
                className="brand-img"
                src={BrandLogo}
                alt="Brand Logo for salt and tar" />
            </div>
            <ul className={`nav-menu ${navView ? 'nav-close' : 'nav-open'}`}>
              <li className="nav-link">Home</li>
              <li className="nav-link">About</li>
              <li className="nav-link">Contact</li>
              <li className="nav-link">Support</li>
              <li className="nav-link">Media</li>
              <li className="nav-link">Log In</li>
            </ul>
            <button
              type="button"
              className="menu-button"
              // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
              onClick={() => setNavView(!navView)}
              >
              <img src={NavOpen} alt="Open menu icon" />
            </button>
          </div>
        </div>
      </header>
    </section>
  );
};

export default Navbar;
