module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {},
    colors: {
      'act-primary': '#B5E5FF',
      'primary': '#3E717E',
      'drk-primary': '#40677D',
      'act-secondary': '#FFD7C2',
      'secondary': '#FFB389',
      'drk-secondary': '#FF9256',
    }
  },
  plugins: [],
};
